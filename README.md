# Bfood is an app for restaurant table reservation

# Functionalitati

**Bfood**

**Use:**

- Login/signin
- Cabinet personal
  - Istoricul rezervarilor
    - Anuleaza
    - Edit
    - Confirm
  - Favorite
  - Iesire din cont
  - Profil
  - Lista recenzii

**Pagina principala:**

- Lista cu restaorante
- Contacte
- Contul meu / login

**Pagina localului:**

- Bronez masa alegand pe harta interactiva
- Descrierea localului
- Meniu
- Recenzii

**Rezervarea:**

- Confirm rezervare mail
- Confirmare qr code
- Anulare dupa 15 minute

**Admin:**

- Adauga/ sterge/ editeaza restaorante
